<?php

return [
    'publicFolder' => env('FBSATIS_PUBLIC_FOLDER'),
    'satisFolder' => env('FBSATIS_SATIS_FOLDER'),
    'command' => env('FBSATIS_COMMAND'),

    'gitlab' => [
        'token' => env('FBSATIS_GITLAB_TOKEN'),
    ],
];
