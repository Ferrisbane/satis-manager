<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::any('/gitlab', function (Request $request)
{
    $token = config('fbsatis.gitlab.token');

    $publicFolder = config('fbsatis.publicFolder');
    $satisFolder = config('fbsatis.satisFolder');
    $commandFormat = config('fbsatis.command');

    $input = $request->all();

    if (empty($token)) {
        return response('token mismatch', 401);
    }

    if (empty($request->header('X-Gitlab-Token')) || $request->header('X-Gitlab-Token') != $token) {
        return response('token mismatch', 401);
    }

    if ( ! $request->has('project')) {
        return response('request did not have project data', 401);
    }

    $project = $request->get('project');

    if (empty($project['namespace']) && empty($project['name'])) {
        return response('no project name data', 401);
    }

    $namespace = $project['namespace'];
    $name = $project['name'];

    $sshRepo = 'git@gitlab.com:'.$namespace.'/'.$name.'.git';
    $httpRepo = 'https://gitlab.com/'.$namespace.'/'.$name.'.git';

    $command = vsprintf($commandFormat, [
        $satisFolder,
        $satisFolder,
        $publicFolder
    ]);

    $sshCommand = "$command --repository-url=$sshRepo;";
    $httpCommand = "$command --repository-url=$httpRepo;";

    $sshOutput = exec($sshCommand);
    $httpOutput = exec($httpCommand);

    $now = \Carbon\Carbon::now()->format('Y-m-d-h-i-s');

    Storage::disk('local')->put(
        'fbsatis-gitlab-'.$now.'.txt',
        json_encode([
            'project' => $project,
            'sshRepo' => $sshRepo,
            'httpRepo' => $httpRepo,
            'sshOutput' => $sshOutput,
            'httpOutput' => $httpOutput,
            'sshCommand' => $sshCommand,
            'httpCommand' => $httpCommand,
        ])
    );

    return response("success", 200);
});

